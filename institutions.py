class School:
	def __init__(self, name="LS Galilei", city="Trento, Italy", domain="lsgalilei.org"):
		self.name     = name
		self.location = city
		self._domain  = domain
		

class Classroom:
	def __init__(self, name, room, school):
		self.name   = name
		self.room   = room
		self.school = school
		self.pupils = []
		self.tests  = dict()

	def addPupil(self, pupil):
		self.pupils.append(pupil)


class Pupil:
	def __init__(self, name, surname, classroom):
		self.name      = name
		self.surname   = surname
		self.classroom = classroom
		self._email    = self._makeEmail()

	def _makeEmail(self):
		return str(self.name+'.'+self.surname+'@'+
				str(self.classroom.school._domain)).lower()
