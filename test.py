import numpy as np
import pandas as pd
from math import nan as nan

import institutions as inst # here's where School, Classroom, Pupil classes are defined

class test:
	
	def __init__(self, term, nid, date, pupil_name, **params):
		self.term       = term   # e.g. "Trim" or "Pent"
		self.id         = nid    # counter
		self.date       = date
		self.pupil      = pupil_name
		self.parameters = params # parameters of test: {points per exercise as list, maxGrade, minGrade, minPass}
		self.marks      = []     # list of points per exercise
		self.tot        = nan    # sum all marks and normalize to maxGrade, hence grade in [0, maxGrade]
		self.grade      = nan    # grade in [minGrade, 10]

	def setParameters(self, **params):
		self.parameters = params

	def assess(self, *marks):
		self.marks = marks
		self._totGrade()
		self._mapGrade()

	def _totGrade(self):
		self.tot = self.parameters["maxGrade"]*sum(self.marks)/sum(self.parameters["pointsEx"])
	
	def _mapGrade(self):
		slope = (1 - self.parameters["minGrade"]/self.parameters["minPass"])
		(self.tot >= self.parameters["minPass"]) and self.grade = self.tot or self.grade = slope*self.tot + self.parameters["minGrade"]
		

